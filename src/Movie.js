import React from "react";
import { directive } from "@babel/types";

const Movie = ({ location }) => {
  const { movie } = location.state;
  console.log(movie);
  return (
    <div>
      <h1>{movie.title}</h1>
      <img src={movie.backdrop_path} alt={movie.title}></img>
      <p>{movie.tagline}</p>
    </div>
  );
};

export default Movie;
