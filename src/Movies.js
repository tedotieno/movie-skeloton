import React from "react";
import { Link } from "@reach/router";
import data from "./trending.json";

function Movies() {
  const movies = data.results;
  return (
    <div>
      <h1>Movies</h1>
      {movies.map(movie => (
        <div key={movie.title}>
          <Link to={`/movie/${movie.id}`} state={{ movie }}>
            {movie.title}
          </Link>
        </div>
      ))}
    </div>
  );
}

export default Movies;
