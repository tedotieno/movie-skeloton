import React from "react";
import { Router } from "@reach/router";
import "./App.css";
import Movies from "./Movies";
import Movie from "./Movie";

function App() {
  return (
    <div className="App">
      <header>This is the header of my app</header>
      <Router>
        <Movies path="/" />
        <Movie path="/movie/:movieId" />
      </Router>
    </div>
  );
}

export default App;
